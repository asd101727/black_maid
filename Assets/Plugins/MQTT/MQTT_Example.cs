using UnityEngine;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System.Collections;
using System.Collections.Generic;

public class MQTT_Example : MonoBehaviour
{
    MqttClient client;
    //開一個客戶端

    byte Base = MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE;
    //QOS決定訊息穩定度與延遲時間，將決定好的廣播態度取名為「Base」

    //QOS_LEVEL_EXACTLY_ONCE 代表廣播這頻道的訊息時，會確認自己一定有確實廣播到一次，甚至會多次
    //QOS_LEVEL_AT_LEAST_ONCE 代表廣播這頻道的訊息時，只會隨便廣播一下，但會保證有廣播到一次
    //QOS_LEVEL_AT_MOST_ONCE 代表廣播這頻道的訊息時，只會隨便廣播一下，甚至不保證有廣播到一次

    [Header("頻道名稱")]
    public string Channel = "大中天";
    //頻道名稱，取甚麼名字都可以

    [Header("伺服器網址")]
    public string Url = "broker.emqx.io";

    string[] Array = new string[2];
    //單純用來切割收到訊息的
    int delay = 0;
    void Start()
    {
        if (PlayerPrefs.GetString("GUID") == "")//如果你沒有GUID
        {
            PlayerPrefs.SetString("GUID", System.Guid.NewGuid().ToString());
            //給你一個GUID，用來判斷訊息對象
        }

        client = new MqttClient(Url, 1883, false, null);
        //這個客戶端使用 broker.emqx.io 網域（免費伺服器的意思）

        client.Connect(PlayerPrefs.GetString("GUID"));
        //客戶端註冊...隨便一個名稱就好，不重要

        if (!client.IsConnected)//如果沒有連線到
        {
            print("連線失敗");
            return;
        }

        client.Subscribe(new string[] { Channel }, new byte[] { Base });
        //訂閱名為「Channel」的頻道

        Array[0] = "";
        StartCoroutine(Delay());
        StartCoroutine(ClearStart());
    }

    public void Say(string what)//廣播
    {
        client.Publish(Channel, System.Text.Encoding.UTF8.GetBytes(PlayerPrefs.GetString("GUID") + "/" + what), Base, true);
        //在頻道「Channel」發出廣播，內容是你的 GUID 與「InputSay」中的文字，中間用「/」隔開
    }
    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)//從有訂閱的頻道收到訊息了
    {
        if (e.Topic == Channel)//如果收到訊息的頻道名為「Channel」(可能會訂閱多個頻道，這裡是在辨認來源的頻道)
        {
            Array = System.Text.Encoding.UTF8.GetString(e.Message).Split('/');
            //用"/"符號切割收到的訊息
        }
    }
    void OnDestroy()
    {
        // 斷開連接並釋放資源
        if (client != null && client.IsConnected)
        {
            client.Disconnect();
        }
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Say(delay.ToString());
        }
        if (Array[0] != "")//如果 Array[0] 不是空的，代表有收到訊息了
        {
            string Tcolor = "";
            //文字的顏色，用來快速判斷是你還是別人在說話

            if (Array[0] == PlayerPrefs.GetString("GUID"))//如果訊息是你發出的
                Tcolor = "<color=#6C6C6C>";
            else
                Tcolor = "<color=#000000>";

            Array[0] = "";
            //把 Array[0] 清空

            print(delay + "  " + Tcolor + Array[1] + "</color>");
            //把收到的訊息顯示出來
        }
    }
    IEnumerator ClearStart()
    {
        yield return new WaitForSeconds(0.5f);
        Array[0] = "";
        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
        //當我收到任何訊息時，執行 client_MqttMsgPublishReceived()
    }
    IEnumerator Delay()
    {
        delay += 1;
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(Delay());
    }
}
