﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GAS))]
public class GAS_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GAS myScript = (GAS)target;
        GUILayout.Space(10f); // 这里的值可以根据需要调整
        if (GUILayout.Button("將貼上區變成Code\n（這個按鈕只能在非Play模式時使用）"))
        {
            myScript.SplitAndMakeClassCode();
        }
        GUILayout.Space(10f); // 这里的值可以根据需要调整
        if (GUILayout.Button("下載GAS並轉為Json\n（這個按鈕只能在Play模式時使用）"))
        {
            myScript.DownloadGAS();
        }
    }
}
