using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Showing : MonoBehaviour
{
    public static Showing ShowingI;
    private void Awake()
    {
        ShowingI = this;
    }
    public bool Go = false;
    public Transform Talk;
    public IEnumerator PlayShow(TheShow who)
    {
        string conta = "";
        for (int a = 0; a < who.Shows.Count; a++)
        {
            string sh = who.Shows[a];
            conta = "<Talk>";
            if (sh.Contains(conta))
            {
                sh = sh.Replace(conta, "");
                string[] Spr = sh.Split(",");
                yield return StartCoroutine(Talking(Spr[0], Spr[1]));
            }
            conta = "<Talk>";
            if (sh.Contains(conta))
            {
                sh = sh.Replace(conta, "");
                string[] Spr = sh.Split(",");
                yield return StartCoroutine(Talking(Spr[0], Spr[1]));
            }
        }
        yield return new WaitForSeconds(0);
    }
    public IEnumerator Talking(string Talker, string Say, string Type = "默認")//講話，Type：默認、無法跳過、不等待
    {
        Talk.GetComponent<Image>().DOKill();
        Talk.Find("TalkBox").Find("TalkText").GetComponent<Text>().DOKill();
        string lan = "zh_TW";//當前語言
        float speed = 0.75f;//文字顯示速度
        if (lan == "ja-JP")
            speed *= 1.5f;
        if (lan == "en")
            speed *= 2.5f;
        float wait = Say.Length / 7f / speed + 1f;
        if (Type == "不等待")
            wait = 0;

        if (Say != "")
        {
            //非說話者變黑
            if (wait > 0)
            {
            }

            Talk.gameObject.SetActive(true);
            Talk.GetComponent<Image>().DOFade(1, 0.4f);
            Talk.Find("TalkBox").GetComponent<Image>().color = new Color(0, 0, 0, 1f);
            Talk.Find("NameBox").Find("NameText").GetComponent<Text>().text = Talker;
            Talk.Find("TalkBox").Find("TalkText").GetComponent<Text>().text = "";
            Talk.Find("TalkBox").Find("TalkText").GetComponent<Text>().DOText(Say, Say.Length / 25f / speed).SetEase(Ease.Linear);
            Talk.DOScaleX(1, 0.5f).SetEase(Ease.OutExpo);
            if (Type == "默認")
                //yield return StartCoroutine(ReturnTool.WaitForKeyOrDelayText(wait, Talk.Find("SayText").GetComponent<Text>(), Say));
            //else
                yield return new WaitForSeconds(wait);
        }
        else
        {
            Talk.Find("NameBox").Find("NameText").GetComponent<Text>().text = "";
            Talk.Find("TalkBox").Find("TalkText").GetComponent<Text>().text = "";
            Talk.GetComponent<Image>().DOFade(0f, 0.2f);
            Talk.Find("TalkBox").GetComponent<Image>().color = new Color(Talk.GetComponent<Image>().color.r, Talk.GetComponent<Image>().color.g, Talk.GetComponent<Image>().color.b, 0);
            Talk.GetComponent<Image>().color = new Color(Talk.GetComponent<Image>().color.r, Talk.GetComponent<Image>().color.g, Talk.GetComponent<Image>().color.b, 0);
            yield return new WaitForSeconds(0.2f);
            Talk.gameObject.SetActive(false);
        }
    }
}
