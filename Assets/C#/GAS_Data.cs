using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GAS_Data : MonoBehaviour
{
    public TheSystem Systems;
    public static GAS_Data GAS_DataI;
    private void Awake()
    {
        GAS_DataI = this;
    }
}
[Serializable]
public class TheSystem
{
    [SerializeField]
    private List<TheVar> vars = new List<TheVar>();
    public List<TheVar> Vars
    {
        get { return vars; }
        set { vars = value; }
    }
    [SerializeField]
    private List<TheCon> cons = new List<TheCon>();
    public List<TheCon> Cons
    {
        get { return cons; }
        set { cons = value; }
    }
    [SerializeField]
    private List<TheShow> shows = new List<TheShow>();
    public List<TheShow> Shows
    {
        get { return shows; }
        set { shows = value; }
    }
}
[Serializable]
public class TheVar
{
    [SerializeField] private string name = ""; 
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    [SerializeField] private string type = ""; 
    public string Type
    {
        get { return type; }
        set { type = value; }
    }
}

[Serializable]
public class TheCon
{
    [SerializeField] private string name = ""; 
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    [SerializeField] private string conDec = ""; 
    public string ConDec
    {
        get { return conDec; }
        set { conDec = value; }
    }
    [SerializeField] private List<string> varCon = new List<string>(); 
    public List<string> VarCon
    {
        get { return varCon; }
        set { varCon = value; }
    }
    [SerializeField] private List<string> varREMFirst = new List<string>(); 
    public List<string> VarREMFirst
    {
        get { return varREMFirst; }
        set { varREMFirst = value; }
    }
    [SerializeField] private List<string> varJuage = new List<string>(); 
    public List<string> VarJuage
    {
        get { return varJuage; }
        set { varJuage = value; }
    }
    [SerializeField] private List<string> varCheck = new List<string>(); 
    public List<string> VarCheck
    {
        get { return varCheck; }
        set { varCheck = value; }
    }
    [SerializeField] private List<string> varAndOr = new List<string>(); 
    public List<string> VarAndOr
    {
        get { return varAndOr; }
        set { varAndOr = value; }
    }
}

[Serializable]
public class TheShow
{
    [SerializeField] private string name = ""; 
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    [SerializeField] private List<string> shows = new List<string>(); 
    public List<string> Shows
    {
        get { return shows; }
        set { shows = value; }
    }
}

