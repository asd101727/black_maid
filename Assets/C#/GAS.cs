﻿using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO.Compression;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;

public class GAS : MonoBehaviour
{
    //d
    [Header("GAS網址")]
    public string Url = "";

    [Header("GAS_Data路徑（不用給Assets和自身檔名）")]
    public string GAS_Data_Path = ""; // 指定要编辑的.cs文件的路径

    [Header("下載中")]
    public bool Loading = false;

    [Header("下載列表（不要亂動）")]
    public List<TheGetSheet> GetSheets = new List<TheGetSheet>();

    [Header("懶人貼上區")]
    [Multiline(5)]
    public string AllSheets = "";
    string AllCodes = "";

    [HideInInspector]
    public GAS_Data GAS_Data;
    int[,] Arrays;//抓取時分類用
    int seconds = 0;//當前時間秒數
    void Awake()
    {
        GAS_Data = transform.GetChild(0).GetComponent<GAS_Data>();
        UsingJson("資料庫載入");
    }
    public void DownloadGAS()//下載GAS資料並轉為Json
    {
        if (!EditorApplication.isPlaying)
        {
            Debug.Log("★ 非Play模式時無法使用");
            return;
        }
        if (Loading)
        {
            Debug.Log("已經在下載了，你不要急，如果載太久就手動把Loading按鈕關掉");
            return;
        }
        Debug.Log("開始下載GAS資料");
        Loading = true;
        StartCoroutine(CheckGetGAS_End());
        for (int a = 0; a < GetSheets.Count; a++)
        {
            GetSheets[a].ShowName = GetSheets[a].ShowName.Replace(" 正在下載中", "");
            GetSheets[a].ShowName = GetSheets[a].ShowName.Replace(" 下載完成", "");
            StartCoroutine(GetGAS(GetSheets[a].Name));
        }
    }
    public IEnumerator GetGAS(string Download)//下載GAS的程式碼
    {
        GAS_Data.Systems = new TheSystem();
        GetSheets.Find(z => z.Name == Download).ShowName = Download + " 正在下載中";
        WWWForm form = new WWWForm();
        form.AddField("method", Download);//讀取
        using (UnityWebRequest www = UnityWebRequest.Post(Url, form))                                                                                                                                                                                                                                                                                                                                                                                                                                                   //Excel網址
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)//跳錯判定
                Debug.LogWarning(www.error);
            else
            {
                string str = www.downloadHandler.text;//定義str字串
                string[] Array = str.Split(new string[] { "<&&>" }, StringSplitOptions.None);
                int MostArray = 0;//每筆資料的欄數
                if (!int.TryParse(Array[0], out MostArray))
                {
                    Debug.LogError("找不到工作表：" + Download);
                }
                int Count = Array.Length / MostArray - 1;//最大資料數除以每筆資料的欄數等同有幾排資料
                int elsex = 2;//自動修正
                List<string> title = new List<string>();//獲取資料名稱
                for (int a = 0; a <= Count; a++)
                {
                    int d = (a * MostArray) + elsex;//把數字整理一下
                    if (a == 0)
                    {
                        for (int b = 0; b < MostArray - elsex; b++)
                        {
                            title.Add(Array[d + b]);
                        }
                    }
                    else//這裡以下就要依據各工作表不同來抓資料
                    {
                        object da = CreateInstance(Download);
                        for (int b = 0; b < title.Count; b++)
                        {
                            if (title[b].Contains("(s)"))
                                SetPropertyValue(da, title[b].Replace("(s)", ""), Array[d + b]);
                            else if (title[b].Contains("(slist)"))
                                SetPropertyValue(da, title[b].Replace("(slist)", ""), Array[d + b]);
                            else if (title[b].Contains("(list)"))
                                SetPropertyValue(da, title[b].Replace("(list)", ""), Array[d + b]);
                            else if (title[b].Contains("(i)"))
                            {
                                if (Array[d + b] != "")
                                    SetPropertyValue(da, title[b].Replace("(i)", ""), int.Parse(Array[d + b]));
                                else
                                    SetPropertyValue(da, title[b].Replace("(i)", ""), 0);
                            }
                            else if (title[b].Contains("(f)"))
                            {
                                if (Array[d + b] != "")
                                    SetPropertyValue(da, title[b].Replace("(f)", ""), float.Parse(Array[d + b]));
                                else
                                    SetPropertyValue(da, title[b].Replace("(f)", ""), 0);
                            }
                            else if (title[b].Contains("(b)"))
                            {
                                if (Array[d + b] != "")
                                    SetPropertyValue(da, title[b].Replace("(b)", ""), bool.Parse(Array[d + b]));
                                else
                                    SetPropertyValue(da, title[b].Replace("(f)", ""), false);
                            }
                        }
                        SetPropertyValue(GAS_Data.Systems, Download.Replace("The", "") + "s", da);
                    }
                }
                GetSheets.Find(z => z.Name == Download).ShowName = Download + " 下載完成";
            }
        }

    }
    IEnumerator CheckGetGAS_End()//判定是否抓取完畢
    {
        bool Load = true;
        while (Load)
        {
            Load = false;
            for (int a = 0; a < GetSheets.Count; a++)
            {
                if (!GetSheets[a].ShowName.Contains("下載完成"))
                {
                    Load = true;
                    break;
                }
            }
            yield return null;
        }
        Loading = false;
        UsingJson("資料庫儲存");
        Debug.Log("GAS資料下載並儲存完畢");
    }
    void SetPropertyValue(object obj, string propName, object value)//反射出class的值
    {
        if (value == null || value as string == "")// 防止将null或空字符串作为值
            return;
        PropertyInfo propInfo = obj.GetType().GetProperty(propName);
        if (propInfo != null)
        {
            if (typeof(IList).IsAssignableFrom(propInfo.PropertyType))
            {
                IList list = (IList)propInfo.GetValue(obj);
                if (value is IEnumerable && !(value is string)) // 确保value是IEnumerable但不是string，因为string也是IEnumerable
                {
                    foreach (var item in (IEnumerable)value)
                    {
                        // 在这里，你可能需要确保item可以被安全地添加到list中
                        // 例如，如果list是List<string>，确保item是string
                        list.Add(item);
                    }
                }
                else
                    list.Add(value);// 直接添加之前，确保value是期望的类型
            }
            else if (propInfo.CanWrite)
            {
                try// 对于非列表属性，尝试直接设置值
                {
                    propInfo.SetValue(obj, value, null);
                }
                catch (Exception e)
                {
                    Debug.LogError("Error setting property value: " + e.Message);
                }
            }
        }
        else
            Debug.LogError($"Property '{propName}' not found.");
    }

    object CreateInstance(string className)//反射指定名稱的class
    {
        var assembly = AppDomain.CurrentDomain.GetAssemblies();// 获取当前程序集
        foreach (var asm in assembly)// 在所有程序集中查找给定名称的类
        {
            Type type = asm.GetType(className);
            if (type != null)
                return Activator.CreateInstance(type);// 创建这个类的实例
        }
        Debug.LogError($"Class not found: {className}");// 如果没有找到类，记录一个错误并返回null
        return null;
    }
    public void UsingJson(string what)//Json檔存讀檔
    {
        if (!Directory.Exists(Application.streamingAssetsPath))
            Directory.CreateDirectory(Application.streamingAssetsPath);// 如果不存在，创建它
        if (what == "資料庫儲存")
        {
            //Debug.Log("正在儲存...");

            // 將GAS_Data.Systems資料轉換為json
            string jsonData = JsonUtility.ToJson(GAS_Data.Systems);

            // 將json數據使用GZipStream壓縮
            byte[] jsonBytes = Encoding.UTF8.GetBytes(jsonData);

            // 這將打開一個保存文件對話框，用戶可以指定文件的保存位置和名稱
            string path = Application.streamingAssetsPath + "/System.gz";
            if (!string.IsNullOrEmpty(path))
            {
                File.WriteAllText(path, "這裡是要保存的文本或數據");
            }
            using (var gzipStream = new GZipStream(new FileStream(path, FileMode.Create), CompressionMode.Compress))
            {
                gzipStream.Write(jsonBytes, 0, jsonBytes.Length);
                //Debug.Log("儲存完畢");
            }
        }
        if (what == "資料庫載入")//用於非存檔的內建重要資料
        {
            string path = Application.streamingAssetsPath + "/System.gz";
            if (path.Length > 0)
            {
                // 讀取檔案
                if (File.Exists(path))
                {
                    string extension = Path.GetExtension(path);
                    byte[] uncompressedData = null;
                    if (extension == ".gz")
                    {
                        // 讀取 GZipStream 壓縮後的數據
                        using (var gzipStream = new GZipStream(File.OpenRead(path), CompressionMode.Decompress))
                        using (var ms = new MemoryStream())
                        {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = gzipStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, bytesRead);
                            }
                            uncompressedData = ms.ToArray();
                        }
                    }

                    // 將解壓縮後的數據轉換為字串
                    if (uncompressedData != null)
                    {
                        string jsonData = Encoding.UTF8.GetString(uncompressedData);
                        // 將json數據轉換為GAS_Data.Systems資料
                        GAS_Data.Systems = new TheSystem();
                        GAS_Data.Systems = JsonUtility.FromJson<TheSystem>(jsonData);
                    }
                }
                else
                {
                    Debug.Log("找不到資料庫Json檔: " + path);
                }
            }
            else
                Debug.Log("查無路徑: " + path);
        }
    }
    public void SplitAndMakeClassCode()//自動切割並生成ClassCode
    {
        if (EditorApplication.isPlaying)
        {
            Debug.Log("★ Play模式時無法使用");
            return;
        }
        GetSheets.Clear();
        AllCodes = "using System;" + "\n";
        AllCodes += "using System.Collections;" + "\n";
        AllCodes += "using System.Collections.Generic;" + "\n";
        AllCodes += "using UnityEngine;" + "\n\n";
        AllCodes += "public class GAS_Data : MonoBehaviour" + "\n";
        AllCodes += "{" + "\n";
        AllCodes += "    public TheSystem Systems;" + "\n";
        AllCodes += "}" + "\n";
        AllCodes += "[Serializable]" + "\n";
        AllCodes += "public class TheSystem" + "\n";
        AllCodes += "{" + "\n";
        string[] separators = new string[] { "\r\n", "\n" };// 为所有可能的换行符定义一个字符串数组
        string[] lines = AllSheets.Split(separators, StringSplitOptions.None);// 使用string.Split方法按换行符分割字符串
        for (int a = 0; a < lines.Length; a++)
        {
            List<string> c = MakeClassCode(lines[a]);
            if (c.Count > 3)//否則就是空白試算表
            {
                TheGetSheet sh = new TheGetSheet();
                sh.ShowName = c[0];
                sh.Name = c[0];
                sh.ClassCode = "[Serializable]" + "\n";
                sh.ClassCode += "public class " + c[0] + "\n";
                sh.ClassCode += "{" + "\n";
                for (int b = 3; b < c.Count; b++)//0是試算表名稱、1是列數、2是行數，所以才從3開始
                {
                    bool y = false;//如果沒有給資料格式就代表不用進class
                    if (c[b].Contains("(i)"))
                    {
                        y = true;
                        c[b] = c[b].Replace("(i)", "");
                        sh.ClassCode += "    [SerializeField] private int " + FirstLetterToLower(c[b]) + " = 0;" + "\n";
                        sh.ClassCode += "    public int " + c[b] + "\n";
                    }
                    else if (c[b].Contains("(f)"))
                    {
                        y = true;
                        c[b] = c[b].Replace("(f)", "");
                        sh.ClassCode += "    [SerializeField] private float " + FirstLetterToLower(c[b]) + " = 0;" + "\n";
                        sh.ClassCode += "    public float " + c[b] + "\n";
                    }
                    else if (c[b].Contains("(b)"))
                    {
                        y = true;
                        c[b] = c[b].Replace("(b)", "");
                        sh.ClassCode += "    [SerializeField] private bool " + FirstLetterToLower(c[b]) + " = false;" + "\n";
                        sh.ClassCode += "    public bool " + c[b] + "\n";
                    }
                    else if (c[b].Contains("(slist)"))
                    {
                        y = true;
                        c[b] = c[b].Replace("(slist)", "");
                        sh.ClassCode += "    [SerializeField] private List<string> " + FirstLetterToLower(c[b]) + " = new List<string>(); " + "\n";
                        sh.ClassCode += "    public List<string> " + c[b] + "\n";
                    }
                    else if (c[b].Contains("(s)"))
                    {
                        y = true;
                        c[b] = c[b].Replace("(s)", "");
                        sh.ClassCode += "    [SerializeField] private string " + FirstLetterToLower(c[b]) + " = \"\"; " + "\n";
                        sh.ClassCode += "    public string " + c[b] + "\n";
                    }
                    if (y)
                    {
                        sh.ClassCode += "    {" + "\n";
                        sh.ClassCode += "        get { return " + FirstLetterToLower(c[b]) + "; }" + "\n";
                        sh.ClassCode += "        set { " + FirstLetterToLower(c[b]) + " = value; }" + "\n";
                        sh.ClassCode += "    }" + "\n";
                    }
                }
                sh.ClassCode += "}" + "\n";
                AllCodes += "    [SerializeField]" + "\n";
                AllCodes += "    private List<" + c[0] + "> " + FirstLetterToLower(c[0].Replace("The", "")) + "s = new List<" + c[0] + ">();" + "\n";
                AllCodes += "    public List<" + c[0] + "> " + c[0].Replace("The", "") + "s" + "\n";
                AllCodes += "    {" + "\n";
                AllCodes += "        get { return " + FirstLetterToLower(c[0].Replace("The", "")) + "s; }" + "\n";
                AllCodes += "        set { " + FirstLetterToLower(c[0].Replace("The", "")) + "s = value; }" + "\n";
                AllCodes += "    }" + "\n";
                GetSheets.Add(sh);
            }
        }
        AllCodes += "}" + "\n";
        for (int a = 0; a < GetSheets.Count; a++)
        {
            AllCodes += GetSheets[a].ClassCode + "\n";
        }
        EditFileContent(AllCodes);
    }
    List<string> MakeClassCode(string input)//自動切除Excel上複製下來的class生成用字串
    {
        List<string> cleanedParts = new List<string>();// 清理结果，移除所有空白项
        string trimmedInput = input.Trim();// 去除字符串开头和结尾的所有空白字符
        string[] parts = Regex.Split(trimmedInput, @"\s+");// 使用正则表达式分割字符串，匹配一个或多个连续的空白字符
        foreach (string part in parts)
        {
            if (!string.IsNullOrWhiteSpace(part))
                cleanedParts.Add(part);
        }
        return cleanedParts;
    }
    string FirstLetterToLower(string str)//第一個字母小寫
    {
        if (string.IsNullOrEmpty(str) || char.IsLower(str[0]))
            return str;
        return char.ToLower(str[0]) + str.Substring(1);
    }
    void EditFileContent(string str)
    {
        string Path = Application.dataPath + "/" + GAS_Data_Path + "/GAS_Data.cs";
        if (File.Exists(Path))
        {
            string fileContent = File.ReadAllText(Path);// 读取.cs文件的内容
            fileContent = str;
            File.WriteAllText(Path, fileContent);// 将编辑后的内容写回文件
            DateTime now = DateTime.Now; // 获取当前时间
            seconds = now.Second; // 获取当前时间的秒数
            StartCoroutine(UpdateCodeDone());
            AssetDatabase.Refresh();// 刷新资产数据库，使更改生效
        }
        else
            Debug.LogError("Script file not found at path: " + Path);
    }
    IEnumerator UpdateCodeDone()
    {
        var logEntries = System.Type.GetType("UnityEditor.LogEntries,UnityEditor.dll");// 获取Unity编辑器的日志条目
        var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);// 获取"Clear()"方法
        Debug.Log("懶人貼上區沒有會影響程式碼的更動，因此Code不更新");
        yield return new WaitForSeconds(0);
        DateTime now = DateTime.Now; // 获取当前时间
        if (Math.Abs(now.Second - seconds) <= 1)
        {
            clearMethod.Invoke(null, null);// 调用清除方法
            Debug.Log("偵測到懶人貼上區有變動，Code更新中...");
            yield break;
        }
    }
}
[Serializable]
public class TheGetSheet
{
    public string ShowName = "";
    [HideInInspector]
    public string ClassCode = "";
    [HideInInspector]
    public string Name = "";
}