using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewUse : MonoBehaviour
{
    [Header("自動更新")]
    public bool AutoChange = false;

    public enum Type
    {
        Horizontal,
        Vertical,
    }
    [Header("垂直橫向")]
    public Type TheType;
    void OnEnable()
    {
        ChangeSize();
    }
    void ChangeSize()
    {
        Transform bt = transform.GetChild(0).GetChild(0);
        int count = 0;
        for (int a = 0; a < bt.childCount; a++)
        {
            if (bt.GetChild(a).gameObject.activeInHierarchy)
            {
                count += 1;
            }
        }

        if (TheType == Type.Horizontal)
        {
            float Base = bt.GetComponent<GridLayoutGroup>().cellSize.y + bt.GetComponent<GridLayoutGroup>().spacing.y;
            float Base2 = bt.parent.parent.GetComponent<RectTransform>().sizeDelta.y;
            bt.GetComponent<RectTransform>().sizeDelta = new Vector2(0, Base2 + (count - (int)(Base2 / Base)) * Base);
        }
        if (TheType == Type.Vertical)
        {
            float Base = bt.GetComponent<GridLayoutGroup>().cellSize.x + bt.GetComponent<GridLayoutGroup>().spacing.x;
            float Base2 = bt.parent.parent.GetComponent<RectTransform>().sizeDelta.x;
            bt.GetComponent<RectTransform>().sizeDelta = new Vector2(Base2 + (count - (int)(Base2 / Base)) * Base, 0);
        }
        if (AutoChange)
        {
            Invoke("ChangeSize", 0.1f);
        }
    }
}
